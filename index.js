let num=2;
const getCube = Math.pow(num,3);
console.log(`The cube of ${num} is ${getCube}`);

const fullAddress = [258, "Ave", "NW", "California", 90011];
const [first, second, third, fourth, fifth] = fullAddress	;
console.log(`I live at ${first} ${second} ${third}, ${fourth} ${fifth}`);


let myPet = {
	name: 'Lolong',
	type: 'Salwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 3 in',
};

let {name, type, weight, measurement} = myPet;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measaurement of ${measurement}.`);


class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myNewDog = new Dog ('Luca', 5, 'Aspin' );
console.log(myNewDog);
